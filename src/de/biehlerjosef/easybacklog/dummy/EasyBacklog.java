package de.biehlerjosef.easybacklog.dummy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.methods.HttpGet;

import de.biehlerjosef.unofficialeasybacklogadapter.domain.Account;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Backlog;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Domain;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Theme;
import de.biehlerjosef.unofficialeasybacklogadapter.rest.IEasyBacklog;
import de.biehlerjosef.yappeasybacklogtest.TempStatic;

public class EasyBacklog implements IEasyBacklog {

	private String user = "user";
	private String pwd = "pwd";
	private String userName = null;
	private String password = null;


	private boolean isAuthenticated() {
		return userName != null && password != null;
	}


	@Override
	public void setBaseUrl(String base) {
		// TODO Auto-generated method stub

	}


	@Override
	public String getBaseUrl() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setApiKey(String key) {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean authenticate() {
		return true;
	}


	@Override
	public void setAccountId(Integer id) {
		// TODO Auto-generated method stub

	}


	@Override
	public Integer getAccountId() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Theme> getThemes(Backlog backlog) {
		Theme t1 = new Theme();
		t1.setBacklogId(1);
		t1.setCode("T1");
		t1.setCreatedAt("2012-05-25T17:11:20Z");
		t1.setName("Test Theme");
		t1.setPosition(1);
		t1.setUpdatedAt("2012-05-25T17:11:20Z");

		Theme t2 = new Theme();
		t2.setBacklogId(1);
		t2.setCode("T1");
		t2.setCreatedAt("2012-05-25T17:11:20Z");
		t2.setName("Test Theme");
		t2.setPosition(1);
		t2.setUpdatedAt("2012-05-25T17:11:20Z");

		List<Theme> themes = new ArrayList<Theme>();
		themes.add(t1);
		themes.add(t2);
		return themes;
	}


	@Override
	public Theme getTheme(Backlog backlog, Integer themeId) {
		Theme t2 = new Theme();
		t2.setBacklogId(1);
		t2.setCode("T1");
		t2.setCreatedAt("2012-05-25T17:11:20Z");
		t2.setName("Test Theme");
		t2.setPosition(1);
		t2.setUpdatedAt("2012-05-25T17:11:20Z");
		return t2;
	}


	public List<Backlog> getBacklogs() {
		Backlog b1 = new Backlog();
		b1.setAccountId(1);
		b1.setArchived(false);
		b1.setCompanyId(1);
		b1.setCreatedAt("2012-05-25T17:11:20Z");
		b1.setId(1);
		b1.setName("Backlog 1");
		b1.setRate(5);
		b1.setScoringRuleId(1);
		b1.setUpdatedAt("2012-05-25T17:11:20Z");

		Backlog b2 = new Backlog();
		b2.setAccountId(1);
		b2.setArchived(false);
		b2.setCompanyId(1);
		b2.setCreatedAt("2012-05-25T17:11:20Z");
		b2.setId(1);
		b2.setName("Backlog 1");
		b2.setRate(5);
		b2.setScoringRuleId(1);
		b2.setUpdatedAt("2012-05-25T17:11:20Z");


		List<Backlog> backlogs = new ArrayList<Backlog>();
		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);		backlogs.add(b1);
		backlogs.add(b2);
		return backlogs;
	}


	@Override
	public List<Account> getAccounts() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getApiKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
