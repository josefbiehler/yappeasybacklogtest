package de.biehlerjosef.yappeasybacklogtest;

public class LongRunningClass {
	public static int runLong() {
		int retVal = 0;
		for(int i = 0; i < 1000000; i++) {
			for(int o = 0; o < 10000; o++) {
				retVal++;
			}
		}
		return retVal;
	}
}	
