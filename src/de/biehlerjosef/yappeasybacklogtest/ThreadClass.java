package de.biehlerjosef.yappeasybacklogtest;

import android.app.Activity;
import android.widget.Toast;

public class ThreadClass extends Activity implements Runnable{
	
	private Activity ctx;
	
	public ThreadClass(Activity ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void run() {
		int r = LongRunningClass.runLong();
		System.out.println(r);
		final Activity a = this;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(ctx, "asd", Toast.LENGTH_LONG).show();
				System.out.println("asdf");
			}
			
		});
	}
}
