package de.biehlerjosef.yappeasybacklogtest;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Backlog;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Domain;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.IDomain;

public class DomainListViewAdapter extends ArrayAdapter<Domain> {

	private List<? extends Domain> domainObjects;
	private Activity context;

	public DomainListViewAdapter(Activity context, List<? extends Domain> list) {
		super(context, R.layout.domain_object_layout);

		this.context = context;
		this.domainObjects = list;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (convertView == null) {
			LayoutInflater inf = context.getLayoutInflater();
			row = inf.inflate(R.layout.domain_object_layout, parent, false);
			TextView tv1 = (TextView)row.findViewById(R.id.textView1);
			TextView tv2 = (TextView)row.findViewById(R.id.textView2);
			TextView tv3 = (TextView)row.findViewById(R.id.textView3);
			row.setTag(R.id.textView1, tv1);
			row.setTag(R.id.textView2, tv2);
			row.setTag(R.id.textView3, tv3);
		}

		String[] desc = domainObjects.get(position).getDomainDescription();

		((TextView)row.getTag(R.id.textView1)).setText(desc[0]);
		if (desc.length > 1)
			((TextView)row.getTag(R.id.textView2)).setText(desc[1]);
		if (desc.length > 2)
			((TextView)row.getTag(R.id.textView3)).setText(desc[2]);

		return row;
	}
}
