package de.biehlerjosef.yappeasybacklogtest;

import java.util.List;

import de.biehlerjosef.easybacklog.dummy.EasyBacklog;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Backlog;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Domain;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainMenu extends Activity {
	private Activity thatInstance = this;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		EasyBacklog b = new EasyBacklog();
		BacklogListAdapter adp = new BacklogListAdapter(this, b.getBacklogs());
		ListView lv = (ListView)findViewById(R.id.listViewBacklogs);
		lv.setAdapter(adp);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Backlog b = (Backlog) arg0.getItemAtPosition(position);
				
				Intent intent = new Intent(thatInstance, BacklogDetail.class);
				intent.putExtra("backlogId", b.getId());
				
				thatInstance.startActivity(intent);	
			}

		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inf = getMenuInflater();
		inf.inflate(R.layout.menu_test, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public void onListItemClick() {

	}
}
