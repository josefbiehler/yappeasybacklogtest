package de.biehlerjosef.yappeasybacklogtest;

import de.biehlerjosef.easybacklog.dummy.EasyBacklog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText viewUserName;
	private EditText viewPassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		System.out.println("onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		viewUserName = (EditText)findViewById(R.id.editUserName);
		viewPassword = (EditText)findViewById(R.id.editPassword);
	}
	
	public void showMainMenu(boolean showAlreadyAuthenticated) {
		if (showAlreadyAuthenticated) {
			Toast.makeText(this, R.string.loginAlreadyAuthenticated, Toast.LENGTH_SHORT).show();
		}
		Intent intent = new Intent(this, MainMenu.class);
		startActivity(intent);
	}
	
	public void login(View button) {
		String userName = viewUserName.getText().toString();
		String password = viewPassword.getText().toString();
		Boolean b = null;
		try {
			b = (new task()).execute().get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (b != null && b == true) {
			Toast.makeText(this, "authentifiziert", Toast.LENGTH_LONG).show();
		}
		
		showMainMenu(false);
	}
	
	class task extends AsyncTask<String, String, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			EasyBacklog bl = new EasyBacklog();
			bl.setBaseUrl("https://easybacklog.com/api/");
			bl.setApiKey("gws1m20n8tatotvx7riq");
			bl.setAccountId(3432);
			return bl.authenticate();
		}
		
	}
}
