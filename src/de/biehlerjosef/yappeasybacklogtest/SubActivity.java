package de.biehlerjosef.yappeasybacklogtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SubActivity  extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent returnValue = new Intent();
		returnValue.putExtra("return", 25);
		setResult(Activity.RESULT_OK, returnValue);
		finish();
	}
}
