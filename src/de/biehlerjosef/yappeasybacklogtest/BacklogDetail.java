package de.biehlerjosef.yappeasybacklogtest;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class BacklogDetail extends Activity {

	private TextView textView_backlogId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.backlog_detail);
		textView_backlogId = (TextView)findViewById(R.id.backlog_detail_id);

		textView_backlogId.setText(String.valueOf(this.getIntent().getExtras().getInt("backlogId")));
	}
	
	public void testimplizitintent(View view) {
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:1234"));
		startActivity(intent);
	}
	
	public void starteSub(View btn) {
		Intent intent = new Intent(this, SubActivity.class);
		startActivityForResult(intent, 1);
	}
	
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		if(reqCode == 1 && resultCode == this.RESULT_OK) {
			Toast.makeText(this, String.valueOf(data.getExtras().getInt("return")), Toast.LENGTH_LONG).show();
		}
		if (reqCode == 2) {
			Toast.makeText(this, data.getData().getEncodedPath(), Toast.LENGTH_LONG).show();
		}
	
	}
	
	public void starteImplizitMitResult(View btn) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("text/*");
		this.startActivityForResult(intent, 2);
	}

}
