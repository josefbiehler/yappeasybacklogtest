package de.biehlerjosef.yappeasybacklogtest;

public class TempStatic {
	public static String un = null;
	public static String pwd = null;
	public static String baseUrl = null;
	
	public static boolean isAuthenticated() {
		return un != null && pwd != null;
	}
	
	public static void set(String u, String p, String bu) {
		un = u;
		pwd = p;
		baseUrl = bu;
	}
	
	public static void unset() {
		un = null;
		pwd = null;
		baseUrl = null;
	}
}
