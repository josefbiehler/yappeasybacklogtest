package de.biehlerjosef.yappeasybacklogtest;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.biehlerjosef.unofficialeasybacklogadapter.domain.Backlog;

public class BacklogListAdapter extends ArrayAdapter<Backlog> {

	private List<Backlog> backlogs;
	private Activity context;

	public BacklogListAdapter(Activity context, List<Backlog> list) {
		// Base class does not need id of ressource
		super(context, 0);
		this.context = context;
		this.backlogs = list;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (convertView == null) {
			LayoutInflater inf = context.getLayoutInflater();
			row = inf.inflate(R.layout.backlog_list_item, parent, false);
			TextView backlogName = (TextView)row.findViewById(R.id.backlog_list_item_textView_name);
			TextView lastUpdated = (TextView)row.findViewById(R.id.backlog_list_item_textView_lastUpdated);

			row.setTag(R.id.backlog_list_item_textView_lastUpdated, lastUpdated);
			row.setTag(R.id.backlog_list_item_textView_name, backlogName);
		}

		Backlog b = backlogs.get(position);
		((TextView)row.getTag(R.id.backlog_list_item_textView_lastUpdated)).setText(b.getUpdatedAt());
		((TextView)row.getTag(R.id.backlog_list_item_textView_name)).setText(b.getName());

		return row;
	}

	// must override getCount() because otherwise i have to pass the list to the super constructor
	// as i removed the "? extends Domain" bullshit i can pass the list to the ArrayAdapter and could remove the getCount()
	@Override
	public int getCount() {
		return (this.backlogs != null)?this.backlogs.size():0;
	}

	@Override
	public Backlog getItem(int pos) {
		return this.backlogs.get(pos);

	}
}
